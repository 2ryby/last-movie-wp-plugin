<?php
namespace Two;

use Two\Utils\HttpUtils;

class LastMovie
{

    protected $url = 'http://2ryby.pl/last';

    /**
     *
     * @var \Two\Utils\UrlReader\ReaderInterface;
     */
    protected $url_reader;

    public function __construct()
    {
        if (HttpUtils::is_ssl()) {
            $this->url = str_replace('http://', 'https://', $this->url);
        }
    }

    /**
     *
     * @return \Two\Utils\UrlReader\ReaderInterface;
     */
    protected function get_url_reader()
    {
        if ($this->url_reader === null) {
            try {
                $this->url_reader = new \Two\Utils\UrlReader\Wordpress();
            } catch (\Exception $e) {
                try {
                    $this->url_reader = new \Two\Utils\UrlReader\Curl();
                } catch (\Exception $e) {
                    $this->url_reader = new \Two\Utils\UrlReader\PhpWrapper();
                }
            }
        }
        return $this->url_reader;
    }

    protected function get_url_data()
    {
        $output = $this->get_url_reader()->read($this->url);
        if (HttpUtils::is_ssl()) {
            $output = str_replace('http://', 'https://', $output);
        }
        return $output;
    }

    /**
     *
     * @return string
     */
    public function render()
    {
        try {
            return $this->get_url_data();
        } catch (\Exception $e) {
            return '';
        }
    }
}
