<?php
namespace Two\Utils\UrlReader;

interface ReaderInterface {
    public function read($url);
}