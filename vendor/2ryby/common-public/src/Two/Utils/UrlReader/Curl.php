<?php
namespace Two\Utils\UrlReader;

class Curl implements ReaderInterface {

    /**
     *
     * @var boolean
     */
    protected $check_ssl = false;

    public function __construct() {
        if (!function_exists('curl_init')) {
            throw new \RuntimeException("CURL extension has to be loaded.");
        }
    }

    /**
     *
     * @param boolean $flag
     * @return \Two\Utils\UrlReader\Curl
     */
    public function set_check_ssl($flag) {
        $this->check_ssl = (bool) $flag;
        return $this;
    }

    /**
     *
     * {@inheritDoc}
     * @see \Two\Utils\UrlReader\ReaderInterface::read()
     */
    public function read($url) {
        $ch = curl_init();
        if ($this->check_ssl !== true) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        }
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $output = curl_exec($ch);
        curl_close($ch);

        if (empty($output)) {
            throw new \UnexpectedValueException("Response body can't be empty.");
        }
        return $output;
    }

}