<?php
namespace Two\Utils;

class HttpUtils
{

    public static function get_client_ip()
    {
        if (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } elseif (isset($_SERVER['HTTP_X_FORWARDED'])) {
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        } elseif (isset($_SERVER['HTTP_FORWARDED_FOR'])) {
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        } elseif (isset($_SERVER['HTTP_FORWARDED'])) {
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        } elseif (isset($_SERVER['REMOTE_ADDR'])) {
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        } else {
            $ipaddress = null;
        }
        return $ipaddress;
    }

    public static function get_ssl()
    {
        if (isset($_SERVER['HTTPS'])) {
            $ssl = $_SERVER['HTTPS'];
        } elseif (isset($_SERVER['HTTP_X_FORWARDED_PORT'])) {
            $ssl = $_SERVER['HTTP_X_FORWARDED_PORT'] == 443 ? 'on' : 'off';
        } elseif (isset($_SERVER['SERVER_PORT'])) {
            $ssl = $_SERVER['SERVER_PORT'] == 443 ? 'on' : 'off';
        } else {
            $ssl = 'off';
        }
        return $ssl;
    }

    /**
     *
     * @return boolean
     */
    public static function is_ssl()
    {
        return self::get_ssl() == 'on';
    }

    public static function to_ssl()
    {
        if (php_sapi_name() != "cli" && self::get_ssl() != 'on') {
            $host = $_SERVER['HTTP_HOST'];
            if (stripos("www.", $host) === 0) {
                $host = ltrim("www.", $host);
            }
            $redirect = 'https://' . $host . $_SERVER['REQUEST_URI'];
            header('HTTP/1.1 301 Moved Permanently');
            header('Location: ' . $redirect);
            exit();
        }
    }
}
