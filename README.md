## 2RYBY.PL

Wordpress Plugin: 2RYBY.PL - najnowszy film.


## Install via Composer

````json

    "repositories": [
        {
            "type": "vcs",
            "url": "https://bitbucket.org/2ryby/common-public.git"
        },	
        {
			"type": "vcs",
			"url": "https://bitbucket.org/2ryby/last-movie-wp-plugin.git"
        }  
    ],
    "require": {
        "2ryby/common-public": "*",
        "2ryby/2ryby-wp-plugin": "*"
    }
	
````

## Download Link

[2ryby-wp-plugin](https://bitbucket.org/2ryby/last-movie-wp-plugin/get/master.zip)