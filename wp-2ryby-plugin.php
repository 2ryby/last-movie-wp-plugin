<?php

/*
Plugin Name: 2RYBY.PL - najnowszy film
Version: 1.0.3
Description: Automatically shows the latest movie from 2ryby.pl YouTube channel.
Author: Mikołaj Fórmański, Marek Gozdalski
Plugin URI: http://2ryby.pl/
*/

/* Version check */
global $wp_version;

$exit_msg = '2ryby plugin requires WordPress 2.6 or newer.
<a href="http://codex.wordpress.org/Upgrading_WordPress">
Please update!</a>';

if (version_compare($wp_version, "2.6", "<=")) {
    exit($exit_msg);
}

if (!class_exists('Two_2RybyVideo')) {
    if (!class_exists("\Two\LastMovie")) {
        include_once implode(DIRECTORY_SEPARATOR, [
            __DIR__,
            'vendor',
            'autoload.php'
        ]);
    }

    class Two_2RybyVideo extends \Two\LastMovie
    {

        const SHORTCODE = '2rybyVideo';

        public function __construct()
        {
            parent::__construct();
            add_shortcode(self::SHORTCODE, [$this, 'render']);
        }
    }

    $Two_2RybyVideo = new Two_2RybyVideo();
}
